# Dialogue app

This aplications allows you to look for dialogues in movies. 


## How to start

* Clone the repository
* Be sure you are executing with NodeJS v10

git clone https://gitlab.com/programaresunamierda/dialogue.git

### Build the project

`cd dialogue`
`npm install`

### Run the project

`npm start`

### Test the project

`npm test`

That would launch unitiy test that are under the folder `test`. And will execute the e2e test that are under the folder `cypress`


## Run using docker

Dialogue app could be used with docker.

# Build the image

`docker build . -t <imagename>:<imageversion>`

# Run the applicattion
You can run the docker application using docker-compose. It would create a MySQL environment and the application. 


Fill the env variables in the .env file:
MYSQL_HOST=db
MYSQL_DATABASE=app
MYSQL_USERNAME=user
MYSQL_PASSWORD=password
MYSQL_ROOT_PASSWORD=rootpassword
CI_REGISTRY_IMAGE=<imagename>
CI_COMMIT_REF_SLUG=<imageversion>

docker-compose up -d  <imagename>


