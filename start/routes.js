'use strict'

/*
|--------------------------------------------------------------------------
| Routes
|--------------------------------------------------------------------------
|
| Http routes are entry points to your web application. You can create
| routes for different URL's and bind Controller actions to them.
|
| A complete guide on routing is available here.
| http://adonisjs.com/docs/4.1/routing
|
*/

/** @type {typeof import('@adonisjs/framework/src/Route/Manager')} */
const Route = use('Route')

Route.get('/','DialogueController.home');
Route.on ('/signup').render('auth.signup');
Route.on ('/login').render('auth.login');

Route.post ('/signup','UserController.create').validator('CreateUser');
Route.post ('/login', 'UserController.login').validator('LoginUser');



Route.get ('/logout', async ({auth, response}) => {
    await auth.logout();
    return response.redirect ('/');
});


Route.get('/movies','MovieController.home')
Route.on ('/movie/add').render('addmovie');
Route.get ('/movie/delete/:id','MovieController.delete');
Route.post ('/movie','MovieController.create');


Route.get('/actors','ActorController.home')
Route.on ('/actor/add').render('addactor');
Route.get ('/actor/delete/:id','ActorController.delete');
Route.post ('/actor','ActorController.create');

Route.get('/search','SearchController.home')
Route.post('/search/word','SearchController.searchWord')

Route.get ('/api/actors/all','ActorController.getActorsList');

Route.get('/fragments','FragmentController.home')
Route.on ('/fragment/add').render('addfragment');
Route.get ('/fragment/delete/:id','FragmentController.delete');
Route.post ('/fragment','FragmentController.create');


//Route.get ('/api/movies/all','MoviesController.getMoviesList');

Route.post('/search/sentence','SearchController.searchSentence')
