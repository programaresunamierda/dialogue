'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class ActorSchema extends Schema {
  up () {
    this.create('actors', (table) => {
      table.increments()
      table.string('name', 254).notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('actors')
  }
}

module.exports = ActorSchema
