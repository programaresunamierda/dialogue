'use strict'

/** @type {import('@adonisjs/lucid/src/Schema')} */
const Schema = use('Schema')

class FragmentSchema extends Schema {
  up () {
    this.create('fragments', (table) => {
      table.increments()
      table.integer('movie_id').unsigned().references('id').inTable('movies')
      table.integer('actor_id').unsigned().references('id').inTable('actors')
      table.string('text', 254).notNullable()
      table.integer('start').notNullable()
      table.integer('duration').notNullable()
      table.timestamps()
    })
  }

  down () {
    this.drop('fragments')
  }
}

module.exports = FragmentSchema
