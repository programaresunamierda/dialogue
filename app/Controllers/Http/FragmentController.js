'use strict'

const Fragment = use('App/Models/Fragment')
const Movie = use ('App/Models/Movie')
const Actor = use ('App/Models/Actor')



class FragmentController {



    async home ({view}) {
       
        //Fetch all fragments
       const fragments = await Fragment.all();

       return view.render('fragments', { fragments: fragments.toJSON() })
    }

    
    async create ({request, auth, response, session}) {
        const {text,start,duration} = request.all();

        const fragment = new Fragment;
        fragment.text = text;
        fragment.start = start;
        fragment.duration = duration;

        const movie = await Movie.find ("1");
        const actor = await Actor.find ("1");

        fragment.movie().associate (movie); 
        fragment.actor().associate (actor);


        await fragment.save();

        session.flash ({message: 'The fragment has been added'});
        return response.redirect ('/fragment/add');
    }

    async delete ({request, auth, response, session, params}) {
        const fragment = await Fragment.find (params.id);

        await fragment.delete();

        session.flash ({message: 'The fragment has been deleted'});
        return response.redirect ('back');
    }

}

module.exports = FragmentController
