'use strict'

const { validate } = use('Validator')
const Movie = use('App/Models/Movie')
const Fragment = use('App/Models/Fragment')

function toHHMMSS(segundos) {
    var sec_num = parseInt(segundos, 10); // don't forget the second param
    var hours   = Math.floor(sec_num / 3600);
    var minutes = Math.floor((sec_num - (hours * 3600)) / 60);
    var seconds = sec_num - (hours * 3600) - (minutes * 60);

    if (hours   < 10) {hours   = "0"+hours;}
    if (minutes < 10) {minutes = "0"+minutes;}
    if (seconds < 10) {seconds = "0"+seconds;}
    return hours+':'+minutes+':'+seconds;
}

class SearchController {
    async home({view}) {
        return view.render('search', {})
    }
    async searchWord({ request, response, session }) {
        // validate form input
        const validation = await validate(request.all(), {
            word: 'required|min:2|max:255'
        })

        // show error messages upon validation fail
        if (validation.fails()) {
            session.withErrors(validation.messages())
                    .flashAll()

            return response.redirect('back')
        }

        //const fragmentsFound = await Fragment.query().with('movie').with('actor').where('text','LIKE','%'+request.input('word')+'%').fetch();
        const fragmentsFound = [];
        fragmentsFound.push(
            { text:'Si',
              start:toHHMMSS('126'),
              startSeconds:126,
              duration:1,
              movie: { title: 'Tiburón', source: 'https://www.youtube.com/watch?v=JvF0ECpkrqw' } ,
              actor: { name: 'Richard Dreyfuss' }
            });
        // Fash success message to session
        session.flash({ notification: 'Resultados' })
        session.flash({ fragments: fragmentsFound })
        return response.redirect('back')
    }

    async searchSentence({ request, response, session }) {
        // validate form input
        const validation = await validate(request.all(), {
            sentence: 'required|min:2|max:255',
            actor: 'required'
        })

        // show error messages upon validation fail
        if (validation.fails()) {
            session.withErrors(validation.messages())
                    .flashAll()

            return response.redirect('back')
        }

        //const fragmentsFound = await Fragment.query().with('movie').with('actor').where('text','LIKE','%'+request.input('word')+'%').fetch();
        const fragmentsFound = [];
        fragmentsFound.push(
            { text:'Si',
              start:toHHMMSS('126'),
              startSeconds:126,
              duration:1,
              movie: { title: 'Tiburón', source: 'https://www.youtube.com/watch?v=JvF0ECpkrqw' } ,
              actor: { name: 'Richard Dreyfuss' }
            });
        fragmentsFound.push(
            { text:'Dispare',
                start:toHHMMSS('173'),
                startSeconds:173,
                duration:1,
                movie: { title: 'Tiburón', source: 'https://www.youtube.com/watch?v=JvF0ECpkrqw' } ,
                actor: { name: 'Richard Dreyfuss' }
            });
    
        // Fash success message to session
        session.flash({ notification: 'Resultados' })
        session.flash({ fragments: fragmentsFound })
        return response.redirect('back')
    }
}

module.exports = SearchController
