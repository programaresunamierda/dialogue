'use strict'

const Movie = use('App/Models/Movie')

class MovieController {

    async home ({view}) {
       
        //Fetch all movies
       const movies = await Movie.all();

       return view.render('movies', { movies: movies.toJSON() })
    }

    
    async create ({request, auth, response, session}) {
        const {title, source} = request.all();

        const movie = new Movie;
        movie.title = title;
        movie.source = source;
        movie.save();

        session.flash ({message: 'The movie has been added'});
        return response.redirect ('/movie/add');
    }

    async delete ({request, auth, response, session, params}) {
        const themovie = await Movie.find (params.id);

        await themovie.delete();

        session.flash ({message: 'The movie has been deleted'});
        return response.redirect ('back');
    }


}

module.exports = MovieController
