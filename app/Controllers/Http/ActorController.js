'use strict'

const Actor = use('App/Models/Actor')


class ActorController {
    async home ({view}) {
       
        //Fetch all actors
       const actors = await Actor.all();

       return view.render('actors', { actors: actors.toJSON() })
    }

    
    async create ({request, auth, response, session}) {
        const {name} = request.all();

        const actor = new Actor;
        actor.name = name;
        actor.save();

        session.flash ({message: 'The actor has been added'});
        return response.redirect ('/actor/add');
    }

    async delete ({request, auth, response, session, params}) {
        const actor = await Actor.find (params.id);

        await actor.delete();

        session.flash ({message: 'The actor has been deleted'});
        return response.redirect ('back');
    }

    async getActorsList ({request, auth, response, session, params}) {
        const actors = await Actor.all();
        return response.send(actors); 
        
    }
}

module.exports = ActorController
