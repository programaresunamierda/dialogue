'use strict'

class LoginUser {
  get rules () {
    return {
      // validation rules
      'email':'required',
      'password':'required'
    }
  }
  get messages () {
    return {
      'required' : 'Wow now, {{field}} is requierd'
    }
  }

  async fails(error) {
    this.ctx.session.withErrors (error).flashAll();

    return this.ctx.response.redirect('back');
  }
}

module.exports = LoginUser
