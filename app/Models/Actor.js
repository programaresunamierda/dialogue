'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Actor extends Model {
    fragments() {
        return this.belongsToMany('App/Models/Fragment');
    }
}

module.exports = Actor
