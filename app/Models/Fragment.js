'use strict'

/** @type {typeof import('@adonisjs/lucid/src/Lucid/Model')} */
const Model = use('Model')

class Fragment extends Model {
    movie() {
        return this.belongsTo('App/Models/Movie')
    }
    actor() {
        return this.belongsTo('App/Models/Actor')
    }

}

module.exports = Fragment
